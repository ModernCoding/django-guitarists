import { Controller } from "stimulus";


export default class extends Controller {

  connect() {
    this.data.set("pathname", window.location.pathname);
    let pathname = this.data.get("pathname");

    $('[data-pathname]').each(function(){

      switch (true) {

        case pathname == "/" && $(this).attr('data-pathname') == "/":
          
          $(this)
            .addClass('btn-info')
            .removeClass('btn-outline-light');

          break;


        case pathname == "/" && $(this).attr('data-pathname') != "/":
        case $(this).attr('data-pathname') == "/":
          
          $(this)
            .removeClass('btn-info')
            .addClass('btn-outline-light');

          break;


        case pathname.includes($(this).attr('data-pathname')):
          
          $(this)
            .addClass('btn-info')
            .removeClass('btn-outline-light');

          break;


        default:
          
          $(this)
            .removeClass('btn-info')
            .addClass('btn-outline-light');

          break;

      }

    });
    
  }

}