import { Controller } from "stimulus";
import { resize } from "../resizing";


export default class extends Controller {

  connect() {
    console.log(`Hello!`);
  }


  show(e) {

    let $guitarist = $(e.target).closest('.my-guitarist');

    fetch(this.data.get("url"))
      .then(response => response.text())
      
      .then(html => {
        $('.my-guitarist').removeClass('d-none');
        $guitarist.addClass('d-none');
        
        $("#my-guitarist-detail")
          .removeClass('d-none')
          .html(html);
        
        $('html, body').animate({ scrollTop: 0 }, 600);
        resize();
      });
  
  }


  hide() {
    $("#my-guitarist-detail").addClass('d-none').html(null);
    $('.my-guitarist').removeClass('d-none');
    $('html, body').animate({ scrollTop: 0 }, 600);
    resize();
  }

}