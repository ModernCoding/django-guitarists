const common = require("./webpack.common");
const merge = require('webpack-merge');

const TerserPlugin = require('terser-webpack-plugin');

const OptimizeCssAssetsPlugin = require(
  'optimize-css-assets-webpack-plugin'
);


module.exports = merge(
  common,
  
  {
    mode: "production",

    optimization: {
      minimizer: [
        new OptimizeCssAssetsPlugin(),
        new TerserPlugin()
      ]
    }
  }
);