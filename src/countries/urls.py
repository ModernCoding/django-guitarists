from django.urls import path

from .views import (
    CountryCreateView,
    CountryDetailView,
    CountryListView,
    CountryUpdateView,
    CountryDeleteView
  )


app_name = 'countries'

urlpatterns = [
  path('', CountryListView.as_view(), name='list'),
  path('<int:id>/', CountryDetailView.as_view(), name='detail'),
  path('create/', CountryCreateView.as_view(), name='create'),
  path('<int:id>/update', CountryUpdateView.as_view(), name='update'),
  path('<int:id>/delete', CountryDeleteView.as_view(), name='delete')
]
