from django import forms
from .models import Country

class CountryModelForm(forms.ModelForm):
  
  title = forms.CharField(
      widget = forms
          .TextInput(attrs = { "placeholder": "Country name" })
    )

  
  class Meta:
    model = Country
    fields = ['title']


  # def clean_title(self, *args, **kwargs):

  #   title = self.cleaned_data.get('title')

  #   if not "CFE" in title:
  #     raise forms.ValidationError("This is not a valid title")
    
  #   if not "news" in title:
  #     raise forms.ValidationError("This is not a valid title")

  #   return title