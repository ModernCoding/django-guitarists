from django.db import models
from django.urls import reverse

# Create your models here.
class Country(models.Model):

  label = models.CharField(max_length=120)

  def get_absolute_url(self):
    return reverse("countries:detail", kwargs={ "id": self.id })

  def get_update_url(self):
    return reverse("countries:update", kwargs={ "id": self.id })

  def get_delete_url(self):
    return reverse("countries:delete", kwargs={ "id": self.id })


  class Meta:
    verbose_name_plural = "countries"