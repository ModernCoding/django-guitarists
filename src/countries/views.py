from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from django.views.generic import (
  CreateView,
  DetailView,
  ListView,
  UpdateView,
  DeleteView
)

from .models import Country
from .forms import CountryModelForm


# Create your views here.

class CountryListView(ListView):

  template_name = 'countries/list.html'
  queryset = Country.objects.all()


class CountryDetailView(DetailView):
  
  template_name = 'countries/detail.html'

  def get_object(self):
    return get_object_or_404(Country, id=self.kwargs.get("id"))


class CountryCreateView(CreateView):
  
  template_name = 'countries/form.html'
  form_class = CountryModelForm
  queryset = Country.objects.all()

  def form_valid(self, form):
    print(form.cleaned_data)
    return super().form_valid(form)

  def get_success_url(self):
    return reverse('countries:list')


class CountryUpdateView(UpdateView):
  
  template_name = 'countries/form.html'
  form_class = CountryModelForm

  def form_valid(self, form):
    print(form.cleaned_data)
    return super().form_valid(form)

  def get_object(self):
    return get_object_or_404(Country, id=self.kwargs.get("id"))


class CountryDeleteView(DeleteView):
  
  template_name = 'countries/delete.html'

  def get_object(self):
    return get_object_or_404(Country, id=self.kwargs.get("id"))

  def get_success_url(self):
    return reverse('countries:list')