from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from django.views.generic import (
  DetailView,
  ListView,
  UpdateView,
  DeleteView
)

from .models import Guitarist
from .forms import GuitaristModelForm
from countries.models import Country


# Function based views

def guitarist_create_view(request):

  form = GuitaristModelForm(request.POST or None)


  if form.is_valid():

    guitarist = Guitarist()
    guitarist.fullname = request.POST['fullname']
    guitarist.description = request.POST['description']
    
    guitarist.url = request.POST['url'].replace(
        'watch?v=',
        'embed/'
      )
    
    guitarist.country = Country \
        .objects \
        .get(id = request.POST['country_id']) \

    guitarist.save()


  return render(
      request,
      "guitarists/form.html",
      
      {
        "form": form,
        "action": "guitarists:create",
        "redirect": "guitarists:list",
        "countries": Country.objects.all()
      }
    )


# Class based views

class GuitaristListView(ListView):

  template_name = 'guitarists/list.html'
  queryset = Guitarist.objects.order_by('fullname')


class GuitaristDetailView(DetailView):
  
  template_name = 'guitarists/detail.html'


  def get_object(self):

    try:
      guitarist = Guitarist \
          .objects \
          .select_related('country') \
          .get(id = self.kwargs.get("id"))

    except Guitarist.DoesNotExist:
      raise Http404

    return guitarist


class GuitaristUpdateView(UpdateView):
  
  template_name = 'guitarists/form.html'
  form_class = GuitaristModelForm

  def form_valid(self, form):
    print(form.cleaned_data)
    return super().form_valid(form)

  def get_object(self):
    return get_object_or_404(Guitarist, id=self.kwargs.get("id"))


class GuitaristDeleteView(DeleteView):
  
  template_name = 'guitarists/delete.html'

  def get_object(self):
    return get_object_or_404(Guitarist, id=self.kwargs.get("id"))

  def get_success_url(self):
    return reverse('guitarists:list')