from django import forms
from .models import Guitarist

class GuitaristModelForm(forms.ModelForm):
  
  fullname = forms.CharField(
      widget = forms
          .TextInput(
            attrs = {
                "placeholder": "Guitarist fullname",
                "class": "form-control"
              }
          )
    )
  
  url = forms.CharField(
      label="Youtube link",

      widget = forms
          .TextInput(
            attrs = {
                "placeholder": "Youtube link",
                "class": "form-control"
              }
          )
    )
  
  description = forms.CharField(
      required=False,

      widget=forms
          .Textarea(
            attrs = {
                "placeholder": "Your description",
                "class": "form-control",
                "rows": 4
              }
        )
    )

  country_id = forms.IntegerField(label="Nationality")

  
  class Meta:
    model = Guitarist
    fields = ['fullname', 'description', 'country_id', 'url']