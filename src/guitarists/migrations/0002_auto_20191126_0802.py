# Generated by Django 2.2.7 on 2019-11-26 08:02

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('guitarists', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='guitarist',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='guitarist',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
