from django.db import models
from django.urls import reverse
from countries.models import Country

# Create your models here.
class Guitarist(models.Model):

  fullname = models.CharField(max_length=1000)
  url = models.CharField(max_length=1000, blank=True, null=True)
  description = models.TextField(blank=True, null=True)
  country = models.ForeignKey(Country, on_delete=models.PROTECT)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)


  def get_absolute_url(self):
    return reverse("guitarists:detail", kwargs={ "id": self.id })

  def get_update_url(self):
    return reverse("guitarists:update", kwargs={ "id": self.id })

  def get_delete_url(self):
    return reverse("guitarists:delete", kwargs={ "id": self.id })
