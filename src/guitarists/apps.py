from django.apps import AppConfig


class GuitaristsConfig(AppConfig):
    name = 'guitarists'
