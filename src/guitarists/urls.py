from django.urls import path

from .views import (
    guitarist_create_view,
    GuitaristDetailView,
    GuitaristListView,
    GuitaristUpdateView,
    GuitaristDeleteView
  )


app_name = 'guitarists'

urlpatterns = [
  path('', GuitaristListView.as_view(), name='list'),
  path('<int:id>/', GuitaristDetailView.as_view(), name='detail'),
  path('create/', guitarist_create_view, name='create'),
  
  path(
    '<int:id>/update',
    GuitaristUpdateView.as_view(),
    name='update'
  ),
  
  path(
    '<int:id>/delete',
    GuitaristDeleteView.as_view(),
    name='delete'
  )
]
